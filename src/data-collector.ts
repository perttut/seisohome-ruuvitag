// @ts-ignore
import noble from '@abandonware/noble';
import { parseRawV1Ruuvi, RuuviTagDataFormat3 } from './parser';

export class RuuviMeasurement {
  constructor(public timestamp: number, public ruuviId: string, public data: RuuviTagDataFormat3) {
  }
}

export class DataCollector {
  private readonly ruuviIdToMeasurement = new Map<string, RuuviMeasurement>();

  startScanning(): void {
    noble.on('discover', (peripheral: any): void =>
      this.onDiscover(peripheral));
    noble.on('warning', (message: string) => console.warn('warning', message));
    noble.on('stateChange', (state: string) => {
      console.log('state changed', state);
      if (state === 'poweredOn') {
        noble.startScanning([], true, (error: any) => {
          if (error) {
            console.error('scanning error', error);
          }
        });
      }
    });
  }

  getLatestMeasurements(timestamp: number): Array<RuuviMeasurement> {
    return [...this.ruuviIdToMeasurement.values()].filter(rm => rm.timestamp > timestamp);
  }

  private onDiscover(peripheral: any): void {
    const manufacturerData = peripheral.advertisement ? peripheral.advertisement.manufacturerData : undefined;
    // handle only RuuviTags (0499) using RAWv1 format (03)
    if (manufacturerData
      && manufacturerData.length === 16
      && manufacturerData[0] === 0x99
      && manufacturerData[1] === 0x04
      && manufacturerData[2] === 0x03) {
      const rtId = peripheral.id;
      const parsedData = parseRawV1Ruuvi(peripheral.advertisement.manufacturerData.toString('hex'));
      this.ruuviIdToMeasurement.set(rtId, new RuuviMeasurement(Date.now(), rtId, parsedData));
    }
  }
}
