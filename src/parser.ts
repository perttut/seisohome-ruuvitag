export class RuuviTagDataFormat3 {
    /**
     *
     * @param humidity          Humidity with accuracy of 0.5%.
     * @param temperature       Temperature in celsius with two decimal accuracy.
     * @param pressure          Pressure in Pascal.
     * @param accelerationX     Acceleration X in mG.
     * @param accelerationY     Acceleration Y in mG.
     * @param accelerationZ     Acceleration Z in mG.
     * @param voltage           Voltage in mV, practically 1800 ... 3600 mV.
     */
    constructor(
        public humidity: number,
        public temperature: number,
        public pressure: number,
        public accelerationX: number,
        public accelerationY: number,
        public accelerationZ: number,
        public voltage: number) {
    }
}

/**
 * Parses RAWv1 (Data Format 3 Protocol Specification.
 * Original implementation copied from https://github.com/ojousima/node-red/blob/master/ruuvi-node/ruuvitag.js
 * under BSD 3 license.
 *
 * @param manufacturerDataString    RuuviTag manufacturer data as string.
 */
export const parseRawV1Ruuvi = (manufacturerDataString: string): RuuviTagDataFormat3 => {
    const humidityStart      = 6;
    const humidityEnd        = 8;
    const temperatureStart   = 8;
    const temperatureEnd     = 12;
    const pressureStart      = 12;
    const pressureEnd        = 16;
    const accelerationXStart = 16;
    const accelerationXEnd   = 20;
    const accelerationYStart = 20;
    const accelerationYEnd   = 24;
    const accelerationZStart = 24;
    const accelerationZEnd   = 28;
    const voltageStart       = 28;
    const voltageEnd         = 32;

    const humidityString = manufacturerDataString.substring(humidityStart, humidityEnd);
    const humidity = parseInt(humidityString, 16) / 2;

    const temperatureString = manufacturerDataString.substring(temperatureStart, temperatureEnd);
    let temperature = parseInt(temperatureString.substring(0, 2), 16); // Full degrees
    temperature += parseInt(temperatureString.substring(2, 4), 16) / 100; // Decimals
    if (temperature > 128) { // Ruuvi format, sign bit + value
        temperature = temperature - 128;
        temperature = 0 - temperature;
    }
    // TODO: is this necessary?
    // temperature = +temperature.toFixed(2); // Round to 2 decimals, format as a number

    // Ruuvi value 0 means 50 kPa
    const pressure = parseInt(manufacturerDataString.substring(pressureStart, pressureEnd), 16) + 50000; // uint16_t pascals

    let accelerationX = parseInt(manufacturerDataString.substring(accelerationXStart, accelerationXEnd), 16); // milli-g
    if (accelerationX > 32767) { accelerationX -= 65536; } // two's complement

    let accelerationY = parseInt(manufacturerDataString.substring(accelerationYStart, accelerationYEnd), 16); // milli-g
    if (accelerationY > 32767) { accelerationY -= 65536; } // two's complement

    let accelerationZ = parseInt(manufacturerDataString.substring(accelerationZStart, accelerationZEnd), 16); // milli-g
    if (accelerationZ > 32767) { accelerationZ -= 65536; } // two's complement

    const voltage = parseInt(manufacturerDataString.substring(voltageStart, voltageEnd), 16); // milli Volt

    return new RuuviTagDataFormat3(humidity, temperature, pressure, accelerationX, accelerationY, accelerationZ, voltage);
};
