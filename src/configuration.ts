import { DynamoDB } from 'aws-sdk';
import { AttributeMap, ScanInput } from 'aws-sdk/clients/dynamodb';
import { isNullOrUndefined } from 'util';

/**
 * Units used in the system.
 * TODO: move units to common repository later
 */
export enum Unit {
  Celcius = 'C',
  Pascal = 'Pa',
  Humidity = 'H%',
  // TODO: acceleration should be relative to axis (X,Y,Z). Or each axis should be independent "sensor"
  // Acceleration = 'mG',
  Voltage = 'V',
  KWh = 'kWh'
}

// TODO: move SensorConfig class to common repository
export class SensorConfig {
  /**
   * @param id          User assigned id of the sensor. Unique in the system.
   * @param name        Human readable name of the sensor
   * @param hardwareId  The hardware id of the sensor, if any.
   * @param unit        Unit of the sensor reading, for example Celsius, Pascal, Voltage, kWh
   * @param enabled     Is sensor enabled.
   */
  constructor(public id: string, public name: string, public hardwareId: string, public unit: Unit, public enabled: boolean) {
  }
}

// TODO: remove this once these test values are no longer needed for initialization
//   new SensorConfig('freezerTemp', 'Pakastin lämpö', 'f531187fef9f', Unit.Celcius, true),
//   new SensorConfig('freezerHumidity', 'Pakastin kosteus', 'f531187fef9f', Unit.Humidity, true)]);
export class Configuration {
  private readonly dynamoDb = new DynamoDB({apiVersion: '2012-08-10'});
  private readonly updateInterval = 60 * 1000;
  private sensorConfigs: Array<SensorConfig> = [];

  async startUpdating(): Promise<void> {
    setInterval(async () => {
      try {
        await this.updateSensorConfiguration();
      } catch (error) {
        console.error('Sensor configuration update failed, trying again later...', error);
      }
    }, this.updateInterval);
    await this.updateSensorConfiguration();
  }

  getSensorConfiguration(): Array<SensorConfig> { return this.sensorConfigs; }

  private async updateSensorConfiguration(): Promise<string> {
    // TODO: move sensor config reading to common repository
    const scanInput: ScanInput = {TableName: 'sensor-config'};
    const sensorConfigItems = await this.dynamoDb
      .scan(scanInput)
      .promise();
    if (isNullOrUndefined(sensorConfigItems) || isNullOrUndefined(sensorConfigItems.Items)) {
      console.error('DynamoDB scan returned nothing for configuration');

      return Promise.reject('DynamoDB scan returned nothing for configuration');
    }

    try {
      this.sensorConfigs = sensorConfigItems.Items.map(item => new SensorConfig(
        this.getStringValue('id', item),
        this.getStringValue('name', item),
        this.getStringValue('hardwareId', item),
        this.getUnitValue('unit', item),
        this.getBooleanValue('enabled', item)));

      return 'OK';
    } catch (error) {
      console.error('Unable to read sensor configuration from DynamoDB', error);

      return Promise.reject(`Unable to read sensor configuration from DynamoDB: ${error}`);
    }
  }

  private readonly getStringValue = (property: string, item: AttributeMap): string => {
    if (isNullOrUndefined(item[property]) || isNullOrUndefined(item[property].S)) {
      throw new Error(`Missing configuration value ${property}`);
    }

    return item[property].S as string;
  };

  private readonly getUnitValue =  (property: string, item: AttributeMap): Unit => {
    const stringValue = this.getStringValue(property, item);
    if (!Object
          .values(Unit)
          .includes(stringValue)) {
      throw new Error(`Configuration contains illegal unit value ${stringValue}`);
    }

    return stringValue as Unit;
  };

  private readonly getBooleanValue =  (property: string, item: AttributeMap): boolean => {
    if (isNullOrUndefined(item[property]) || isNullOrUndefined(item[property].BOOL)) {
      throw new Error(`Missing configuration value ${property}`);
    }

    return item[property].BOOL as boolean;
  };
}
