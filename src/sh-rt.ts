/**
 * Seiso home RuuviTag data collector.
 */

import { Configuration } from './configuration';
import { DataCollector } from './data-collector';
import { sendSensors } from './data-sender';
import { unpackRuuviData } from './sensor-data';

const main = async (): Promise<void>  => {
  try {
    console.log('seisohome-ruuvitag started!');
    const configuration = new Configuration();
    try {
      console.log('Fetching initial configuration from DynamoDB');
      await configuration.startUpdating();
    } catch (error) {
      console.error('Unable to update configuration initially, may work later...', error);
    }
    const dataCollector = new DataCollector();
    dataCollector.startScanning();
    const dataCollectInterval = 60 * 1000;
    setInterval(
      async () => {
        const sensorDatas = unpackRuuviData(
          configuration.getSensorConfiguration(), dataCollector.getLatestMeasurements(Date.now() - dataCollectInterval));
        console.log('Received', sensorDatas);
        try {
          await sendSensors(sensorDatas);
        } catch (error) {
          console.error('Sending sensor data failed', error);
        }
      },
      dataCollectInterval);
  } catch (error) {
    console.error('Unexpected error');
    process.exit(255);
  }
}

main()
  .catch(error => console.error('This shouldn\'t happen, unhandled error', error));
