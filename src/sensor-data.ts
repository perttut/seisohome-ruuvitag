import { SensorConfig, Unit } from './configuration';
import { RuuviMeasurement } from './data-collector';

// TODO: move SensorData class to common repository
export class SensorData {
  /**
   * Constructor.
   * @param id          Id of the sensor.
   * @param timestamp   Timestamp the sensor value was stored.
   * @param value       Numeric value of the sensor.
   */
  constructor(public id: string, public timestamp: number, public value: number) {}
}

export const unpackRuuviData = (sensorConfigs: Array<SensorConfig>, measurements: Array<RuuviMeasurement>): Array<SensorData> => {
  const enabledRuuviTagIds = new Set(sensorConfigs
    .filter(sc => sc.enabled)
    .map(sc => sc.hardwareId));
  const enabledMeasurements = measurements.filter(m => enabledRuuviTagIds.has(m.ruuviId));
  const idToMeasurement = enabledMeasurements.reduce((acc, curr) => acc.set(curr.ruuviId, curr), new Map<string, RuuviMeasurement>());
  const sensorDatas = sensorConfigs
    .filter(sc => sc.enabled && idToMeasurement.has(sc.hardwareId))
    .map(sc => {
      const measurement = idToMeasurement.get(sc.hardwareId);
      if (measurement) {
        return mapRuuviToSensor(measurement, sc);
      }

      return undefined;
    })
    .filter(sensorData => sensorData !== undefined);

  return sensorDatas as Array<SensorData>;
};

const mapRuuviToSensor = (measurement: RuuviMeasurement, sensorConfig: SensorConfig): SensorData | undefined => {
  let result: SensorData | undefined;
  switch (sensorConfig.unit) {
    case Unit.Celcius:
      result = new SensorData(sensorConfig.id, measurement.timestamp, measurement.data.temperature);
      break;
    case Unit.Pascal:
      result = new SensorData(sensorConfig.id, measurement.timestamp, measurement.data.pressure);
      break;
    case Unit.Humidity:
      result = new SensorData(sensorConfig.id, measurement.timestamp, measurement.data.humidity);
      break;
    case Unit.Voltage:
      result = new SensorData(sensorConfig.id, measurement.timestamp, measurement.data.voltage);
      break;
    default:
      // should never happen, but let's skip these
      console.warn('Unhandled unit type', sensorConfig.unit);
      result = undefined;
      break;
  }

  return result;
};
