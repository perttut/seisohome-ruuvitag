import { DynamoDB } from 'aws-sdk';
import { PutItemInput } from 'aws-sdk/clients/dynamodb';
import { SensorData } from './sensor-data';

export const sendSensors = async (sensorDatas: Array<SensorData>): Promise<void> => {
  const dynamoDb = new DynamoDB({apiVersion: '2012-08-10'});
  const createPutItem = (sensorData: SensorData): PutItemInput => ({
    TableName: 'sensor',
    Item: {
      id: {S: sensorData.id},
      timestamp: {N: sensorData.timestamp.toString()},
      value: {N: sensorData.value.toString()}
    }
  });
  const putItems: Array<PutItemInput> = sensorDatas.map(createPutItem);
  const sendItems = putItems.map(putItem => dynamoDb
    .putItem(putItem)
    .promise());
  await Promise.all(sendItems);
};
