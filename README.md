# RuuviTag data collector and sender

Script for reading nearby RuuviTag data and sending it to DynamoDB.

## Installation on Ubuntu for local testing

### Install dependencies

```bash
sudo apt-get install bluetooth bluez libbluetooth-dev libudev-dev
```

### Run without root
```bash
sudo setcap cap_net_raw+eip $(eval readlink -f `which node`)
```

### Run locally

```bash
export AWS_REGION=eu-west-1
export AWS_SECRET_ACCESS_KEY=...
export AWS_ACCESS_KEY_ID=...
npm install
npm start
```


## AWS deployment

### DynamoDB

To deploy DynamoDB tables.
```bash
npm run deploy
```

To remove DynamoDB deployment. Note that this removes all the collected data.
```bash
npm run undeploy
```

### Pi user creation

To allow *pi* user (on Raspberry) access DynamoDB, you need to create the user to AWS.
```bash
cd aws
./create_pi_user.sh
```

After creating user, you have a working AWS access key under directory *~/seisohome_keys/pi-dynamodb.credentials*. These credentials are
stored on pi during Ansible deployment.

## Raspberry Pi deployment

These instructions work at least for Raspbian Buster. To execute deployment, you need to have Ansible installed.

First enable SSH key based authentication.

Generate key.
```bash
ssh-keygen -o -a 100 -t ed25519 -f ~/.ssh/pi_id_ed25519
```

Set key into use for pi user on Pi (change IP address).
```bash
ssh-copy-id -i ~/.ssh/pi_id_ed25519.pub pi@192.168.1.19
```

Edit/add *private.inventory* file under *ansible* directory with the following content (make sure you set the right IP for your environment).
```
[pi]
192.168.1.19
```

Change directory to *ansible* and run *install.sh*.

## Querying data

To query collected data using AWS cli, you can use the following. Write query expression attributes to a file
*expression_attributes.json*
```json
{
      ":sensorId": {"S": "freezerTemp"},
      ":startTime": {"N": "1577732454000"},
      ":endTime": {"N":   "1577734554000"}
}
```

Execute query using aws-cli
```bash
aws dynamodb query --table-name sensor \
--key-condition-expression "#sid = :sensorId AND #ts BETWEEN :startTime AND :endTime" \
--expression-attribute-values file://expression_attributes.json \
--expression-attribute-names '{"#sid": "id","#ts": "timestamp"}' > freezerTemp.json
```

Convert the JSON output as CSV
```bash
jq -r '.Items[] | { v: .value.N, t: .timestamp.N} | map(.) | @csv' < freezerTemp.json > freezerTemp.csv
``` 
