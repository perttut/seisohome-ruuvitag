#!/usr/bin/env bash
# Fail immediately in case of error
set -e

aws iam create-user --user-name pi
aws iam put-user-policy --user-name pi --policy-name PiDynamoDBPolicy --policy-document file://pi_policy.json

KEY_DIR=~/seisohome_keys

[[ -d $KEY_DIR ]] || mkdir $KEY_DIR

#######################
# Create IAM access key
#######################
JSON=$(aws iam create-access-key --user-name pi)
# capture group inside " characters
ACCESS_KEY_PAT='AccessKeyId": "([^"]+)"'
SECRET_KEY_PAT='SecretAccessKey": "([^"]+)"'
[[ "$JSON" =~ $ACCESS_KEY_PAT ]]
echo AWS_ACCESS_KEY_ID=${BASH_REMATCH[1]} > $KEY_DIR/pi-dynamodb.credentials
[[ "$JSON" =~ $SECRET_KEY_PAT ]]
echo AWS_SECRET_ACCESS_KEY=${BASH_REMATCH[1]} >> $KEY_DIR/pi-dynamodb.credentials
