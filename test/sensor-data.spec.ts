import { SensorConfig, Unit } from '../src/configuration';
import { RuuviMeasurement } from '../src/data-collector';
import { RuuviTagDataFormat3 } from '../src/parser';
import { SensorData, unpackRuuviData } from '../src/sensor-data';

const sensorConfigs = [
  new SensorConfig('test1', 'sensor 1', 'hwid1', Unit.Celcius, true),
  new SensorConfig('test2', 'test2 name', 'hwid2', Unit.Celcius, false),
  new SensorConfig('test3', 'sensor 2', 'hwid1', Unit.Humidity, true),
  new SensorConfig('test4', 'no data found', 'hwid4', Unit.Humidity, true)
];

const ruuviData1 = new RuuviTagDataFormat3(1, 2, 3, 4, 5, 6, 7);
const ruuviMeasurement1 = new RuuviMeasurement(1000, 'hwid1', ruuviData1);
const ruuviData2 = new RuuviTagDataFormat3(10, 20, 30, 40, 50, 60, 70);
const ruuviMeasurement2 = new RuuviMeasurement(2000, 'hwid2', ruuviData2);

describe('unpackRuuviData', () => {
  test('filter disabled', () => {
    const sensorDatas = unpackRuuviData(sensorConfigs, [ruuviMeasurement2]);
    expect(sensorDatas.length)
      .toBe(0);
  });
  const sensorDatas = unpackRuuviData(sensorConfigs, [ruuviMeasurement1, ruuviMeasurement2]);
  expect(sensorDatas.length)
    .toBe(2);
  const sensor1 = sensorDatas.find(sd => sd.id === 'test1');
  const sensor2 = sensorDatas.find(sd => sd.id === 'test3');
  expect(sensor1)
    .toBeDefined();
  expect((sensor1 as SensorData).value)
    .toBe(2);
  expect(sensor2)
    .toBeDefined();
  expect((sensor2 as SensorData).value)
    .toBe(1);
});
