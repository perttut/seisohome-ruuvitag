import { parseRawV1Ruuvi, RuuviTagDataFormat3 } from '../src/parser';

test('parseRawV1Ruuvi', () => {
  const data = parseRawV1Ruuvi('990403571b03c19affb8ffb504190c13');
  expect(data.humidity)
    .toBe(43.5);
  expect(data.temperature)
    .toBe(27.03);
  expect(data.pressure)
    .toBe(99562);
  expect(data.accelerationX)
    .toBe(-72);
  expect(data.accelerationY)
    .toBe(-75);
  expect(data.accelerationZ)
    .toBe(1049);
  expect(data.voltage)
    .toBe(3091);
});
